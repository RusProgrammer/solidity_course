// SPDX-License-Identifier: UNLICENSED

import "./IDataTypesPractice.sol";
pragma solidity 0.8.12;

contract Exercice1 is IDataTypesPractice {
    bool var1 = true;
    uint8 var2 = 0.5 + 0.5;
    int64 var3 = 3;
    int256 var4 = var3;
    address var5 = address(this);
    bytes3 var6 = 0xaabbcc;
    string var7 = "Hello World!";
    uint40[2] var8 = [1, 3];
    uint256 var9 = var8[0] + var8[1];
    int8 var10 = 20;
    bytes32 var11 = "Hello World!";
    uint256[5] var12 = [1, 2, 3, 4, 5];
    uint256[] var13 =[45678976];

    function getBigUint() external pure returns (uint256){
        uint256 v1 = 1;
        uint256 v2 = 2;

        return ~v2 | ~v1;
    }

    function getInt256() external view returns(int256){
        return var4;
    }
    function getUint256() external view returns(uint256){
        return var9;
    }
    function getIint8() external view returns(int8){
        return var10;
    }
    function getUint8() external view returns(uint8){
        return var2;
    }
    function getBool() external view returns(bool){
        return var1;
    }
    function getAddress() external view returns(address){
        return var5;
    }
    function getBytes32() external view returns(bytes32){
        return var11;
    }
    function getArrayUint5() external view returns(uint256[5] memory){
        return var12;
    }
    function getArrayUint() external view returns(uint256[] memory){
        return var13;
    }
    function getString() external view returns(string memory){
        return var7;
    }
    
}