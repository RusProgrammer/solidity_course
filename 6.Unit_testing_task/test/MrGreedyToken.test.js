const { accounts, toBN } = require("../scripts/utils/utils.js");
const { assert } = require("chai");
const Reverter = require("./helpers/reverter");
const MrGreedyToken = artifacts.require("MrGreedyToken");
const truffleAssert = require("truffle-assertions");

describe("Contracts Hater Token", () => {
  const reverter = new Reverter();

  let OWNER, SOMEBODY, SOMEONE;
  let ANOTHER_SC;
  let ONE_TOKEN;
  let AMOUNT = 123456;
  let mrGreedyToken;
  before("setup", async () => {
    OWNER = await accounts(0);
    SOMEBODY = await accounts(1);
    SOMEONE = await accounts(2);

    mrGreedyToken = await MrGreedyToken.new("MrGreedyToken", "MGT");
    ANOTHER_SC = await MrGreedyToken.new("MrGreedyToken", "MGT");
    ONE_TOKEN = toBN(10).pow(6);
    AMOUNT *= ONE_TOKEN;
    await mrGreedyToken.mint(OWNER, AMOUNT);
    await reverter.snapshot();
  });

  afterEach("revert", async () => {
    await reverter.revert();
  });

  describe("meta data", () => {
    it("name must be 'MrGreedyToken'", async () => {
      assert.equal(await mrGreedyToken.name(), "MrGreedyToken");
    });
    it("symbol must be 'MGT'", async () => {
      assert.equal(await mrGreedyToken.symbol(), "MGT");
    });
  });

  describe("decimals", () => {
    it("decimals must be 6", async () => {
      assert.equal(await mrGreedyToken.decimals(), 6);
    });
  });

  describe("getResultingTransferAmount()", () => {
    it("should decrease amount by 10 tokens", async () => {
      assert.equal(await mrGreedyToken.getResultingTransferAmount(100 * ONE_TOKEN), 90 * ONE_TOKEN);
    });
    it("result always should be >= 0", async () => {
      assert.equal(await mrGreedyToken.getResultingTransferAmount(9 * ONE_TOKEN), 0);
    });
  });

  describe("transfer()", () => {
    it("should transfer amount to enother account", async () => {
      await mrGreedyToken.transfer(SOMEBODY, AMOUNT / 2);

      let ReipientBalance = await mrGreedyToken.balanceOf(SOMEBODY);
      let SomebodyBalance = await mrGreedyToken.balanceOf(SOMEONE);
      let SenderBalance = await mrGreedyToken.balanceOf(OWNER);
      let TreasuryBalance = await mrGreedyToken.balanceOf(await mrGreedyToken.treasury());

      assert.equal(ReipientBalance.toString(), (await mrGreedyToken.getResultingTransferAmount(AMOUNT / 2)).toString());
      assert.equal(SomebodyBalance.toString(), 0);
      assert.equal(SenderBalance.toString(), (AMOUNT - AMOUNT / 2).toString());
      assert.equal(
        TreasuryBalance.toString(),
        (AMOUNT / 2 - (await mrGreedyToken.getResultingTransferAmount(AMOUNT / 2))).toString()
      );
    });

    it("should transfer all amount to treasury if amount <= fee", async () => {
      await mrGreedyToken.transfer(SOMEBODY, 9 * ONE_TOKEN);

      let ReipientBalance = await mrGreedyToken.balanceOf(SOMEBODY);
      let SomebodyBalance = await mrGreedyToken.balanceOf(SOMEONE);
      let SenderBalance = await mrGreedyToken.balanceOf(OWNER);
      let TreasuryBalance = await mrGreedyToken.balanceOf(await mrGreedyToken.treasury());

      assert.equal(
        ReipientBalance.toString(),
        (await mrGreedyToken.getResultingTransferAmount(9 * ONE_TOKEN)).toString()
      );
      assert.equal(SomebodyBalance.toString(), 0);
      assert.equal(SenderBalance.toString(), (AMOUNT - 9 * ONE_TOKEN).toString());
      assert.equal(
        TreasuryBalance.toString(),
        (9 * ONE_TOKEN - (await mrGreedyToken.getResultingTransferAmount(9 * ONE_TOKEN))).toString()
      );
    });
  });
});
