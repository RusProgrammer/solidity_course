const { accounts } = require("../scripts/utils/utils.js");
const { assert } = require("chai");
const Reverter = require("./helpers/reverter");
const ContractsHaterToken = artifacts.require("ContractsHaterToken");
const truffleAssert = require("truffle-assertions");

describe("Contracts Hater Token", () => {
  const reverter = new Reverter();

  let OWNER, SOMEBODY, SOMEONE;
  let ANOTHER_SC;
  let AMOUNT = 123456;
  let contractsHaterToken;
  before("setup", async () => {
    OWNER = await accounts(0);
    SOMEBODY = await accounts(1);
    SOMEONE = await accounts(2);

    contractsHaterToken = await ContractsHaterToken.new("ContractsHaterToken", "CHT");
    ANOTHER_SC = await ContractsHaterToken.new("ContractsHaterToken", "CHT");
    await contractsHaterToken.mint(OWNER, AMOUNT);
    await reverter.snapshot();
  });

  afterEach("revert", async () => {
    await reverter.revert();
  });

  describe("meta data", () => {
    it("name must be 'ContractsHaterToken'", async () => {
      assert.equal(await contractsHaterToken.name(), "ContractsHaterToken");
    });
    it("symbol must be 'CHT'", async () => {
      assert.equal(await contractsHaterToken.symbol(), "CHT");
    });
  });

  describe("decimals", () => {
    it("decimals must be 18", async () => {
      assert.equal(await contractsHaterToken.decimals(), 18);
    });
  });

  describe("transfer()", () => {
    it("should transfer ether to enother account", async () => {
      await contractsHaterToken.transfer(SOMEBODY, AMOUNT / 2);

      let ReipientBalance = await contractsHaterToken.balanceOf(SOMEBODY);
      let SomebodyBalance = await contractsHaterToken.balanceOf(SOMEONE);
      let SenderBalance = await contractsHaterToken.balanceOf(OWNER);

      assert.equal(ReipientBalance, AMOUNT / 2);
      assert.equal(SomebodyBalance, 0);
      assert.equal(SenderBalance, AMOUNT - AMOUNT / 2);
    });
  });

  describe("addToWhitelist()/removeFromWhitelist()", () => {
    it("should revert any transfer sent to a common SC", async () => {
      await truffleAssert.reverts(
        contractsHaterToken.transfer(ANOTHER_SC.address, AMOUNT),
        "To is a SC and Not in WhiteList"
      );
    });

    it("should valid tranfer to a SC in whiteList", async () => {
      await contractsHaterToken.addToWhitelist(ANOTHER_SC.address);
      await truffleAssert.passes(contractsHaterToken.transfer(ANOTHER_SC.address, AMOUNT));
    });

    it("should be possible manipulate with whiteList", async () => {
      await truffleAssert.reverts(
        contractsHaterToken.transfer(ANOTHER_SC.address, AMOUNT),
        "To is a SC and Not in WhiteList"
      );
      await contractsHaterToken.addToWhitelist(ANOTHER_SC.address);
      await truffleAssert.passes(contractsHaterToken.transfer(ANOTHER_SC.address, AMOUNT));
      await contractsHaterToken.removeFromWhitelist(ANOTHER_SC.address);
      await truffleAssert.reverts(
        contractsHaterToken.transfer(ANOTHER_SC.address, AMOUNT),
        "To is a SC and Not in WhiteList"
      );
    });

    it("should get exception, addToWhitelist() only by owner", async () => {
      await truffleAssert.reverts(
        contractsHaterToken.addToWhitelist(ANOTHER_SC.address, { from: SOMEBODY }),
        "Ownable: caller is not the owner"
      );
    });

    it("should get exception, removeFromWhitelist() only by owner", async () => {
      await truffleAssert.reverts(
        contractsHaterToken.removeFromWhitelist(ANOTHER_SC.address, { from: SOMEBODY }),
        "Ownable: caller is not the owner"
      );
    });
  });
});
