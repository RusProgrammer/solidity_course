const { accounts } = require("../scripts/utils/utils.js");
const { assert } = require("chai");
const Reverter = require("./helpers/reverter");
const SimpleToken = artifacts.require("SimpleToken");
const truffleAssert = require("truffle-assertions");

describe("Simple Token", () => {
  const reverter = new Reverter();

  let OWNER, SOMEBODY, SOMEONE;
  let AMOUNT = 123456;
  let simpleToken;
  before("setup", async () => {
    OWNER = await accounts(0);
    SOMEBODY = await accounts(1);
    SOMEONE = await accounts(2);

    simpleToken = await SimpleToken.new("SimpleToken", "ST");
    await reverter.snapshot();
  });

  afterEach("revert", async () => {
    await reverter.revert();
  });

  describe("meta data", () => {
    it("name must be 'SimpleToken'", async () => {
      assert.equal(await simpleToken.name(), "SimpleToken");
    });
    it("symbol must be 'ST'", async () => {
      assert.equal(await simpleToken.symbol(), "ST");
    });
  });

  describe("decimals", () => {
    it("decimals must be 18", async () => {
      assert.equal(await simpleToken.decimals(), 18);
    });
  });

  describe("mint()", () => {
    it("should get exception, mint only by owner", async () => {
      await truffleAssert.reverts(
        simpleToken.mint(SOMEONE, AMOUNT, { from: SOMEBODY }),
        "Ownable: caller is not the owner"
      );
    });

    it("only recipient should get more money", async () => {
      await simpleToken.mint(SOMEONE, AMOUNT, { from: OWNER });
      let ReipientBalance = await simpleToken.balanceOf(SOMEONE);
      let SomebodyBalance = await simpleToken.balanceOf(SOMEBODY);

      assert.equal(ReipientBalance, AMOUNT);
      assert.equal(SomebodyBalance, 0);
    });
  });

  describe("burn()", () => {
    beforeEach("setup", async () => {
      await simpleToken.mint(SOMEONE, AMOUNT, { from: OWNER });
      await simpleToken.mint(SOMEBODY, AMOUNT, { from: OWNER });
    });

    it("anyone could burn", async () => {
      await truffleAssert.passes(simpleToken.burn(AMOUNT, { from: SOMEBODY }));
      await truffleAssert.passes(simpleToken.burn(AMOUNT, { from: SOMEONE }));
    });

    it("after burn amount of money should decrease", async () => {
      let halfOfAmount = Math.round(AMOUNT / 2);
      await simpleToken.burn(halfOfAmount, { from: SOMEONE });
      let SOMEONEBalance = await simpleToken.balanceOf(SOMEONE);
      let SOMEBODYBalance = await simpleToken.balanceOf(SOMEBODY);
      assert.equal(SOMEONEBalance, AMOUNT - halfOfAmount);
      assert.equal(SOMEBODYBalance, AMOUNT);

      await simpleToken.burn(AMOUNT, { from: SOMEBODY });
      SOMEONEBalance = await simpleToken.balanceOf(SOMEONE);
      SOMEBODYBalance = await simpleToken.balanceOf(SOMEBODY);
      assert.equal(SOMEONEBalance, AMOUNT - halfOfAmount);
      assert.equal(SOMEBODYBalance, 0);
    });
  });
});
