// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";

import "./IContractsHaterToken.sol";
import "./SimpleToken.sol";


contract ContractsHaterToken is SimpleToken, IContractsHaterToken {
    using EnumerableSet for EnumerableSet.AddressSet;

    EnumerableSet.AddressSet whiteList;

    constructor(string memory name_, string memory symbol_) SimpleToken(name_, symbol_) {}

    function transfer(address to, uint256 amount) public override returns (bool) {
        require(to.code.length == 0 || whiteList.contains(to), "To is a SC and Not in WhiteList");

        _transfer(_msgSender(), to, amount);
        return true;
    }

    function addToWhitelist(address candidate_) external override onlyOwner {
        whiteList.add(candidate_);
    }

    function removeFromWhitelist(address candidate_) external override onlyOwner {
        whiteList.remove(candidate_);
    }
}
