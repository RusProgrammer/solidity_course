// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./ISimpleToken.sol";

contract SimpleToken is Ownable, ERC20, ISimpleToken {
    constructor(string memory name_, string memory symbol_) ERC20(name_, symbol_) {}

    function mint(address to_, uint256 amount_) external override onlyOwner {
        _mint(to_, amount_);
    }

    function burn(uint256 amount_) external override {
        address owner = _msgSender();
        _burn(owner, amount_);
    }
}
