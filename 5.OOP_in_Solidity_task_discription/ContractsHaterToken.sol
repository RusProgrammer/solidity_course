// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IContractsHaterToken.sol";
import "./SimpleToken.sol";

contract ContractsHaterToken is SimpleToken, IContractsHaterToken {
    address[] whiteList;

    constructor(string memory name_, string memory symbol_) SimpleToken(name_, symbol_) {
    }

    function transfer(address to, uint256 amount) public override returns (bool) {
        uint32 size;
        bool isValid = true;
        assembly {
            size := extcodesize(to)
        }
        if(size > 0){
            isValid = false;
            uint256 _whiteList_length = whiteList.length;
            address[] memory _whiteList = whiteList;
            for (uint256 i = 0; i < _whiteList_length; i++) {
                if (_whiteList[i] == to) {
                    isValid = true;
                }
            }
        }
        require(isValid, "To is a SC and Not in WhiteList");


        address owner = _msgSender();
        _transfer(owner, to, amount);
        return true;
    }
    
    function addToWhitelist(address candidate_) override external onlyOwner{
        whiteList.push(candidate_);
    }

    function removeFromWhitelist(address candidate_) override external onlyOwner{
        address[] memory _whiteList = whiteList;
        uint256 _whiteList_length = _whiteList.length;

        for(uint256 i = 0; i < _whiteList_length; i++){
            if(_whiteList[i] == candidate_){
                whiteList[i] = _whiteList[_whiteList_length - 1];
                whiteList.pop();
                break;
            }
        }
    }
}
