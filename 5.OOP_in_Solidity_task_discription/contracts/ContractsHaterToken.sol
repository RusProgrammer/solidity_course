// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import ".\IContractsHaterToken.sol";
import ".\SimpleToken.sol";

contract ContractsHaterToken is SimpleToken ,IContractsHaterToken {
    address[] whiteList;

    constructor() ERC20("ContractsHaterToken", "CHT") {}

    modifier onlyNotSCOrInWhiteList(address _to) {
        uint32 size;
        bool isValid = true;
        assembly {
            size := extcodesize(_to)
        }
        if(size > 0){
            isValid = false;
            uint256 _whiteList_length = whiteList.length;
            address[] _whiteList = whiteList;
            for (uint256 i = 0; i < _whiteList_length; i++) {
                if (_whiteList[i] == _to) {
                    isValid = true;
                }
            }
        }
        require(isValid, "To is a SC and Not in WhiteList");
        _;
    }

    function transfer(address _to, uint256 _amount) override external returns (bool) onlyNotSCOrInWhiteList(_to) {
        address owner = _msgSender();
        _transfer(owner, to, amount);
        return true;
    }
    
    function addToWhitelist(address candidate_) override external onlyOwner{
        whiteList.push(candidate_);
    }

    function removeFromWhitelist(address candidate_) override external onlyOwner{
        uint256 _whiteList_length = whiteList.length;
        address[] _whiteList = whiteList;

        for(uint256 i = 0; i < _whiteList_length; i++){
            if(_whiteList[i] == candidate_){
                whiteList[i] = _whiteList[_whiteList_length];
                whiteList.pop();
                return;
            }
        }
    }
}
