// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IMrGreedyToken.sol";
import "./SimpleToken.sol";

contract MrGreedyToken is SimpleToken, IMrGreedyToken {
    address _treasury_address = address(this);
    uint256 constant fee = 10;

    constructor(string memory name_, string memory symbol_) SimpleToken(name_, symbol_) {
    }

    function decimals() public view override returns (uint8) {
        return 6;
    }
    
    function treasury() external view returns (address){
        return _treasury_address;
    }

    function getResultingTransferAmount(uint256 amount_) external view returns (uint256){
        uint256 _result_amount = amount_ - fee * 10**uint(decimals());
        return _result_amount >= 0 ? _result_amount : 0;
    }

    function transfer(address to, uint256 amount) public virtual override returns (bool) {
        address owner = _msgSender();

        uint256 fee_amount = fee * (10 ** uint256(decimals()));
        if(amount <= fee_amount){
            _transfer(owner, _treasury_address, amount);
            return true;
        }
        
        uint256 new_amount = amount - fee_amount;
        _transfer(owner, to, new_amount);
        _transfer(owner, _treasury_address, fee_amount);
        return true;
    }
}
