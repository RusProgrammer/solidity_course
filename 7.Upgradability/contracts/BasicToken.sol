// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./interfaces/IBasicToken.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract BasicToken is IBasicToken, OwnableUpgradeable, ERC20Upgradeable {
    function __Token_init(string memory name_, string memory symbol_) external initializer {
        __Ownable_init();
        __ERC20_init(name_, symbol_);
    }

    function mintArbitrary(address recipient_, uint256 amount_) external onlyOwner {
        _mint(recipient_, amount_);
    }
}
