// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";

contract UpgradedToken is OwnableUpgradeable, ERC20Upgradeable {
    function __Token_init(string memory name_, string memory symbol_) external initializer {
        __Ownable_init();
        __ERC20_init(name_, symbol_);
    }

    function mintArbitrary(address _recipient, uint256 _amount) external onlyOwner {
        _mint(_recipient, _amount);
    }

    function burn(address target_, uint256 amount_) external onlyOwner {
        _burn(target_, amount_);
    }
}
