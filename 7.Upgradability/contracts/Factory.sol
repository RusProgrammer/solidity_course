// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./interfaces/IFactory.sol";
import "./BasicToken.sol";
import "@openzeppelin/contracts/proxy/transparent/TransparentUpgradeableProxy.sol";
import "@openzeppelin/contracts/proxy/transparent/ProxyAdmin.sol";

contract Factory is IFactory {
    address internal implementation;

    constructor(address _logic) {
        implementation = _logic;
    }

    function deployUpgradableERC20(
        address owner_
    ) external returns (address proxyAddress_, address adminAdress_) {
        ProxyAdmin admin_ = new ProxyAdmin();
        adminAdress_ = address(admin_);

        TransparentUpgradeableProxy proxy_ = new TransparentUpgradeableProxy(
            implementation,
            adminAdress_,
            ""
        );
        proxyAddress_ = address(proxy_);

        BasicToken proxyAsToken_ = BasicToken(proxyAddress_);
        proxyAsToken_.__Token_init("TestToken", "TT");

        proxyAsToken_.transferOwnership(owner_);
        admin_.transferOwnership(owner_);
    }
}
