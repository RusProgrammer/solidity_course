// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

interface IBasicToken {
    function __Token_init(string memory name_, string memory symbol_) external;

    function mintArbitrary(address recipient_, uint256 amount_) external;
}
