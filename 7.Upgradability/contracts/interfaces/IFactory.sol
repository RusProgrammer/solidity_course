// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

interface IFactory {
    function deployUpgradableERC20(
        address owner_
    ) external returns (address proxyAddress_, address adminAdress_);
}
