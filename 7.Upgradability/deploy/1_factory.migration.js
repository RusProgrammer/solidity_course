const BasicToken = artifacts.require("BasicToken");
const Factory = artifacts.require("Factory");

module.exports = async (deployer) => {
  const basicTokenImpl = await deployer.deploy(BasicToken);

  await deployer.deploy(Factory, basicTokenImpl.address);
};
