const { accounts } = require("../scripts/utils/utils.js");
const Reverter = require("./helpers/reverter");

const { assert } = require("chai");

const Factory = artifacts.require("Factory");
const BasicToken = artifacts.require("BasicToken");
const UpgradedToken = artifacts.require("UpgradedToken");
const ProxyAdmin = artifacts.require("ProxyAdmin");

describe("Factory", () => {
  const reverter = new Reverter();

  let USER2;

  let factory;

  before("setup", async () => {
    USER2 = await accounts(1);

    const basicTokenImpl = await BasicToken.new();
    factory = await Factory.new(basicTokenImpl.address);

    await reverter.snapshot();
  });

  afterEach("revert", async () => {
    await reverter.revert();
  });

  describe("deployUpgradableERC20()", () => {
    it("should correctly deploy new contract ", async () => {
      const data = await factory.deployUpgradableERC20.call(USER2);
      await factory.deployUpgradableERC20(USER2);

      const proxyAddress = data[0];
      const adminAddress = data[1];
      let proxy = await BasicToken.at(proxyAddress);
      let admin = await ProxyAdmin.at(adminAddress);

      assert.equal(await proxy.name(), "TestToken");
      assert.equal(await proxy.symbol(), "TT");

      await proxy.mintArbitrary(USER2, 100, { from: USER2 });
      assert.equal(await proxy.balanceOf(USER2), 100);

      const upgradedTokenImpl = await UpgradedToken.new();
      await admin.upgrade(proxyAddress, upgradedTokenImpl.address, { from: USER2 });

      let proxyUp = await UpgradedToken.at(proxyAddress);
      assert.equal(await proxy.name(), "TestToken");
      assert.equal(await proxy.symbol(), "TT");

      await proxy.mintArbitrary(USER2, 100, { from: USER2 });
      assert.equal(await proxy.balanceOf(USER2), 200);

      await proxyUp.burn(USER2, 50, { from: USER2 });
      assert.equal(await proxy.balanceOf(USER2), 150);
    });
  });
});
