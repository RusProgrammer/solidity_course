const { accounts } = require("../scripts/utils/utils.js");
const Reverter = require("./helpers/reverter");

const { assert } = require("chai");
const truffleAssert = require("truffle-assertions");

const BasicToken = artifacts.require("BasicToken");

describe("Basic Token", () => {
  const reverter = new Reverter();

  let USER1;
  let USER2;

  let basicToken;

  before("setup", async () => {
    USER1 = await accounts(0);
    USER2 = await accounts(1);

    basicToken = await BasicToken.new();
    await basicToken.__Token_init("TestToken", "TT");

    await reverter.snapshot();
  });

  afterEach("revert", async () => {
    await reverter.revert();
  });

  describe("creation", () => {
    it("should get exception if try to init again", async () => {
      await truffleAssert.reverts(
        basicToken.__Token_init("TestToken", "TT"),
        "Initializable: contract is already initialized"
      );
    });

    it("should set parameters correctly", async () => {
      assert.equal(await basicToken.name(), "TestToken");
      assert.equal(await basicToken.symbol(), "TT");
    });
  });

  describe("mintArbitrary()", () => {
    it("should mint tokens", async () => {
      await basicToken.mintArbitrary(USER1, 100);
      assert.equal(await basicToken.balanceOf(USER1), 100);
    });

    it("should revert for not owner", async () => {
      await truffleAssert.reverts(
        basicToken.mintArbitrary(USER1, 100, { from: USER2 }),
        "Ownable: caller is not the owner"
      );
    });
  });
});
