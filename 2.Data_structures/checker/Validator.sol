// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.12;

import "@openzeppelin/contracts/access/Ownable.sol";

interface IDataStructurePractice {
    struct User {
        string name;
        uint256 balance;
        bool isActive;
    }

    function setNewUser(address _userAdr, User calldata _newUser) external;
    function getUser(address _user) external view returns(User memory);
    function getMyInfo() external view returns(User memory);
}

contract DataStructurePracticeValidator {
    function validate(address _toValidate) external returns(bool) {
        require(Ownable(_toValidate).owner() == address(this), "DataStructurePracticeValidator: invalid owner");

        IDataStructurePractice _practice = IDataStructurePractice(_toValidate);

        address _myAddress = 0x9eCd08Fc708cDb77a33AFd83eb7f5ca4E4344766;

        IDataStructurePractice.User memory _me = IDataStructurePractice.User("Krasotulia", 42, true);
        IDataStructurePractice.User memory _this = 
            IDataStructurePractice.User("Honorable Mr Validator", 100500, true);

        _practice.setNewUser(_myAddress, _me);

        IDataStructurePractice.User memory _toCompare1 = _practice.getUser(_myAddress);

        require(_toCompare1.balance == _me.balance, "DataStructurePracticeValidator: getUser invalid balance");
        require(_toCompare1.isActive == _me.isActive, "DataStructurePracticeValidator: getUser invalid isActive");
        require(
            ((keccak256(abi.encodePacked((_toCompare1.name))) 
            == keccak256(abi.encodePacked((_me.name))))), 
            "DataStructurePracticeValidator: getUser invalid name"
        );

        IDataStructurePractice.User memory _toCompare2 = _practice.getUser(address(this));

        require(_toCompare2.balance == _this.balance, "DataStructurePracticeValidator: getUser invalid balance");
        require(_toCompare2.isActive == _this.isActive, "DataStructurePracticeValidator: getUser invalid isActive");
        require(
            ((keccak256(abi.encodePacked((_toCompare2.name))) 
            == keccak256(abi.encodePacked((_this.name))))), 
            "DataStructurePracticeValidator: getUser invalid name"
        );

        IDataStructurePractice.User memory _toCompare3 = _practice.getMyInfo();

        require(_toCompare3.balance == _this.balance, "DataStructurePracticeValidator: getMyInfo invalid balance");
        require(_toCompare3.isActive == _this.isActive, "DataStructurePracticeValidator: getMyInfo invalid isActive");
        require(
            ((keccak256(abi.encodePacked((_toCompare3.name))) 
            == keccak256(abi.encodePacked((_this.name))))), 
            "DataStructurePracticeValidator: getMyInfo invalid name"
        );

        return true;
    }
}