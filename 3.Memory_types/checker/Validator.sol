// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.12;

import "@openzeppelin/contracts/access/Ownable.sol";

interface IMemoryTypesPractice {
    function setA(uint256 _a) external;
    function setB(uint256 _b) external;
    function setC(uint256 _c) external;
    function calc1() external view returns(uint256);
    function calc2() external view returns(uint256);
    function claimRewards(address _user) external;
    function addNewMan(
        uint256 _edge, 
        uint8 _dickSize, 
        bytes32 _idOfSecretBluetoothVacinationChip, 
        uint32 _iq
    ) external;
    function getMiddleDickSize() external view returns(uint256);
    function numberOfOldMenWithHighIq() external view returns(uint256);

    function userInfo() external view returns(address);
    function rewardsClaimed(address) external view returns(bool);
}

interface IUserInfo {
    struct User {
        uint256 balance;
        uint256 unlockTime;
    }

    function addUser(address _user, uint256 _balance, uint256 _unlockTime) external;
    function getUserInfo(address _user) external view returns(User memory);
}

contract MemoryTypesPracticeValidator {
    function validate(IMemoryTypesPractice _practice) external returns(bool) {
        if (
            !validate1(_practice) ||
            !validate2(_practice) ||
            !validate3(_practice) ||
            !validate4(_practice) ||
            !validate5(_practice) ||
            !validate6(_practice)
        ) return false;

        return true;
    }

    function validate1(IMemoryTypesPractice _practice) public returns(bool) {
        _practice.setA(21);
        _practice.setB(5);
        _practice.setC(8);
        require(_practice.calc1{gas: 785}() == 173, "MemoryTypesPracticeValidator: Wrong calc1");

        return true;
    }

    function validate2(IMemoryTypesPractice _practice) public view returns(bool) {
        require(_practice.calc2{gas: 10000}() == 522, "MemoryTypesPracticeValidator: Wrong calc2");

        return true;
    }

    function validate3(IMemoryTypesPractice _practice) public returns(bool) {
        IUserInfo ui = IUserInfo(_practice.userInfo());

        ui.addUser(address(this), 13000, 1);

        _practice.claimRewards{gas:24772}(address(this));
        require(_practice.rewardsClaimed(address(this)), "MemoryTypesPracticeValidator: Wrong claimReward");
        return true;
    }

    function validate4(IMemoryTypesPractice _practice) public returns(bool) {
        _practice.addNewMan{gas: 80000}(20, 15, bytes32("0x1"), 110);
        _practice.addNewMan{gas: 80000}(60, 13, bytes32("0x2"), 137);
        _practice.addNewMan{gas: 80000}(25, 50, bytes32("0x3"), 32);
        _practice.addNewMan{gas: 80000}(19, 21, bytes32("0x4"), 94);

        return true;
    }

    function validate5(IMemoryTypesPractice _practice) public view returns(bool) {
        require(_practice.getMiddleDickSize{gas: 15500}() == 20,
            "MemoryTypesPracticeValidator: wrong getMiddleDickSize");

        return true;
    }

    function validate6(IMemoryTypesPractice _practice) public view returns(bool) {
        require(_practice.numberOfOldMenWithHighIq{gas: 20012}() == 1,
            "MemoryTypesPracticeValidator: wrong numberOfOldMenWithHighIq");

        return true;
    }
}